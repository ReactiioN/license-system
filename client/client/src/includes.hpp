#pragma once
#if !defined(_CRT_SECURE_NO_WARNINGS)
    #define _CRT_SECURE_NO_WARNINGS
#endif

#if !defined(NOMINMAX)
    #define NOMINMAX
#endif

#include <array>
#include <string>
#include <sstream>
#include <vector>

#include <json.hpp>
#include <xorstr.hpp>

#define SODIUM_STATIC 1
#include <sodium.h>

#define CURL_STATICLIB
#include <curl/curl.h>
