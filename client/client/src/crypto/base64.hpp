#pragma once
#include "../includes.hpp"

class base64
{
    /// <summary> 
    /// The binary. 
    /// </summary>
    using binary_t = std::vector<uint8_t>;

public:
    static std::string encode( 
        const std::string_view& data );

    static std::string encode( 
        const binary_t& data );

    static std::string encode( 
        const std::uint8_t* data, 
        std::size_t         len );

    static std::string decode( 
        const std::string_view& input );

    static std::string url_encode( 
        const std::string_view& in );

    static binary_t decode_binary(
        const std::string_view& input );

    template<std::size_t N>
    static std::string array_to_base64( 
        const std::array<std::uint8_t, N>& data );

    template<class T>
    static T base64_to_array( 
        const std::string_view& input );
};

template<size_t N>
std::string base64::array_to_base64( 
    const std::array<std::uint8_t, N>& data )
{
    return encode( data.data(), data.size() );
}

template<class T>
T base64::base64_to_array(
    const std::string_view& input )
{
    T key;
    auto data = decode_binary( input );
    std::copy_n( std::make_move_iterator( data.begin() ), key.size(), key.begin() );
    return key;
}
