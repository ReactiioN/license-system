#pragma once
#include "../includes.hpp"

class Sealedbox
{
    using PublicKey = std::array<std::uint8_t, crypto_box_PUBLICKEYBYTES>;
    using SecretKey = std::array<std::uint8_t, crypto_box_SECRETKEYBYTES>;

public:
    class KeyPair
    {
    public:
        KeyPair();
        void generate();
        void load_public(
            const std::string& key );
        void load_secret(
            const std::string& key );
        std::string get_public_key() const;
        std::string get_secret_key() const;

    public:
        PublicKey public_key{};
        SecretKey secret_key{};
    };

    static std::string encrypt(
        const std::string_view& plain,
        const PublicKey&        public_key );

    static std::string encrypt(
        const std::string_view& plain,
        const KeyPair&          key_pair );

    static std::string decrypt(
        const std::string_view& cipher,
        const PublicKey&        public_key,
        const SecretKey&        secret_key );

    static std::string decrypt(
        const std::string_view& cipher,
        const KeyPair&          key_pair );
};
