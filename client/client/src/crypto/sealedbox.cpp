#include "sealedbox.hpp"
#include "base64.hpp"

Sealedbox::KeyPair::KeyPair()
{
    generate();
}

void Sealedbox::KeyPair::generate()
{
    crypto_box_keypair( public_key.data(), secret_key.data() );
}

void Sealedbox::KeyPair::load_public(
    const std::string& key )
{
    public_key = base64::base64_to_array<PublicKey>( key );
}

void Sealedbox::KeyPair::load_secret(
    const std::string& key )
{
    secret_key = base64::base64_to_array<SecretKey>( key );
}

std::string Sealedbox::KeyPair::get_public_key() const
{
    return base64::array_to_base64( public_key );
}

std::string Sealedbox::KeyPair::get_secret_key() const
{
    return base64::array_to_base64( secret_key );
}

std::string Sealedbox::encrypt(
    const std::string_view& plain,
    const PublicKey&        public_key )
{
    try {
        std::string cipher;
        cipher.resize( plain.size() + crypto_box_SEALBYTES );
        crypto_box_seal(
            reinterpret_cast<std::uint8_t*>( cipher.data() ),
            reinterpret_cast<const std::uint8_t*>( plain.data() ),
            plain.size(),
            public_key.data()
        );
        return base64::encode( cipher );
    }
    catch( ... ) {
        return {};
    }
}

std::string Sealedbox::encrypt(
    const std::string_view& plain,
    const KeyPair&          key_pair )
{
    return encrypt( plain, key_pair.public_key );
}

std::string Sealedbox::decrypt(
    const std::string_view& cipher,
    const PublicKey&        public_key,
    const SecretKey&        secret_key )
{
    try {
        std::string plain;
        auto cipher_dec = base64::decode( cipher );
        plain.resize( cipher_dec.size() - crypto_box_SEALBYTES );
        crypto_box_seal_open(
            reinterpret_cast<std::uint8_t*>( plain.data() ),
            reinterpret_cast<const std::uint8_t*>( cipher_dec.data() ),
            cipher_dec.size(),
            public_key.data(),
            secret_key.data()
        );
        return plain;
    }
    catch( ... ) {
        return {};
    }
}

std::string Sealedbox::decrypt(
    const std::string_view& cipher,
    const KeyPair&          key_pair )
{
    return decrypt( cipher, key_pair.public_key, key_pair.secret_key );
}
