/// Author:                 frk
/// Last modification:      12.22.2016
/// Last modification by:   ReactiioN
#pragma once
#include <cstdint>
#include <utility>

namespace xor_compile_time {

constexpr auto time = __TIME__;
constexpr auto seed = static_cast<std::int32_t>( time[ 7 ] )        +
                      static_cast<std::int32_t>( time[ 6 ] ) * 10   +
                      static_cast<std::int32_t>( time[ 4 ] ) * 60   +
                      static_cast<std::int32_t>( time[ 3 ] ) * 600  +
                      static_cast<std::int32_t>( time[ 1 ] ) * 3600 +
                      static_cast<std::int32_t>( time[ 0 ] ) * 36000;

/// <summary>
/// 1988, Stephen Park and Keith Miller
/// "Random Number Generators: Good Ones Are Hard To Find", considered as "minimal standard"
/// Park-Miller 31 bit pseudo-random number generator, implemented with G. Carta's optimisation:
/// with 32-bit math and without division
/// </summary>
/// 
template<std::int32_t N>
struct RandomGenerator
{
private:
    /// <summary>
    /// 7^5.
    /// </summary>
    static constexpr unsigned a     = 16807;
    /// <summary>
    /// 2^31 - 1.
    /// </summary>
    static constexpr unsigned m     = 2147483647;
    static constexpr unsigned s     = RandomGenerator< N - 1 >::value;
    /// <summary> 
    /// Multiply lower 16 bits by 16807. 
    /// </summary>
    static constexpr unsigned lo    = a * ( s & 0xFFFF );
    /// <summary> 
    /// Multiply higher 16 bits by 16807. 
    /// </summary>
    static constexpr unsigned hi    = a * ( s >> 16 );
    /// <summary> 
    /// Combine lower 15 bits of hi with lo's upper bits. 
    /// </summary>
    static constexpr unsigned lo2   = lo + ( ( hi & 0x7FFF ) << 16 );
    /// <summary>
    /// Discard lower 15 bits of hi. 
    /// </summary>
    static constexpr unsigned hi2   = hi >> 15;
    static constexpr unsigned lo3   = lo2 + hi;

public:
    static constexpr unsigned max   = m;
    static constexpr unsigned value = lo3 > m ? lo3 - m : lo3;
};

template<>
struct RandomGenerator<0>
{
    static constexpr unsigned value = seed;
};

template<std::int32_t N, std::int32_t M>
struct RandomInt
{
    static constexpr auto value = RandomGenerator<N + 1>::value % M;
};

template<std::int32_t N>
struct RandomChar
{
    static const char value = static_cast<char>( 1 + RandomInt<N, 0x7F - 1>::value );
};

template<std::size_t N, std::int32_t K>
struct XorString
{
private:
    const char              m_key;
    std::array<char, N + 1> m_encrypted;

    constexpr char enc( const char c ) const
    {
        return c ^ m_key;
    }

    char dec( const char c ) const
    {
        return c ^ m_key;
    }

public:
    template<std::size_t... Is>
    constexpr __forceinline XorString( const char* str, std::index_sequence<Is...> ) :
        m_key( RandomChar< K >::value ),
        m_encrypted{ enc( str[ Is ] )... }
    {
    }

    __forceinline const char* decrypt()
    {
        for( std::size_t i = 0; i < N; ++i ) {
            m_encrypted[ i ] = dec( m_encrypted[ i ] );
        }
        m_encrypted[ N ] = '\0';
        return m_encrypted.data();
    }
};
}

#ifdef _DEBUG
    #define XORS(s)(s)
#else
    #define XORS(s)(xor_compile_time::XorString<sizeof(s)-1, __COUNTER__>(s, std::make_index_sequence<sizeof(s)-1>()).decrypt())
#endif
